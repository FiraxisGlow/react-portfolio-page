import React, { Component } from 'react';

class Header extends Component {
  render() {
    return (
     <header>
         <div className="logo">
            <h1>Anton Kiri:</h1>
            <h3>Portfolio</h3>
         </div>
         <nav>
             <ul>
                 <li className="first">
                     <a target="_blank" rel="noopener noreferrer" href="https://gitlab.com/FiraxisGlow">Gitlab</a>
                 </li>
                 <li>
                     <a target="_blank" rel="noopener noreferrer" href="https://github.com/firaxisglow">Github</a>
                 </li>
                 <li className="last">
                     <a target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/anton-kiri-9b2856150/">LinkedIn</a>
                 </li>
             </ul>
         </nav>
     </header>
    );
  }
}

export default Header;
