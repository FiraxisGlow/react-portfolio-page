import React, { Component } from 'react';
//import cvFi from '../../Assets/downloads/anton-kiri-cv(fi).pdf';
//import cvEn from '../../Assets/downloads/anton-kiri-cv(en).pdf';
class Footer extends Component {
  render() {
    return (
     <footer>
        <p className="email">Email: kirianton95@gmail.com</p>
        <nav>
          <ul>
            <li className="first">
              <p>Downloads:</p>
            </li>
            <li>
            <a href={process.env.PUBLIC_URL + '/anton-kiri-cv(fi).pdf'} download="anton-kiri-cv(fi).pdf">CV(Fi)</a>
            </li>
            <li className="last">
            <a href={process.env.PUBLIC_URL + '/anton-kiri-cv(en).pdf'} download="anton-kiri-cv(en).pdf">CV(EN)</a>
            </li>
          </ul>
        </nav>
     </footer>
    );
  }
}

export default Footer;
