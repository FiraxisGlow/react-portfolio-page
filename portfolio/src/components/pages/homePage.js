import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import profileImage from '../../Assets/images/antonkiri.png';
class Homepage extends Component {
  render() {
    return (
     <div className="container-fluid">
        <h2>About me:</h2>
        <p>
        I'm a 4th year software engineer student at Jyväskylä University of Applied Sciences (JAMK).<br/>
        I've been a part of several projects on JAMK courses and been a project manager in two of them and a similar figure in a few others.<br/>
        I consider myself a team player and I get things done whenever I can. If not then I'll ask for help or try to figure it out.<br/>
        </p>
        <h2>Programming skills:</h2>
        <p>
        Programming languages that I know include C#, Javascript, PHP, Java and C++.<br/>
        I'm most confident with my C# and JavaScript skills as those are the ones I've worked with the most.<br/>
        I also have some experience in NodeJS, JQuery, React and Sass.<br/>
        </p>
        <h2>Projects I'm proud of:</h2>
        
          <nav>
              <ul>
                  <li className="first">
                  <div className="dropdown">
                     <span><Link to="/Project">Jira Dashboard - Qvantel - Project manager</Link></span>
                     <div className="dropdown-content">
                     <p>
                      This project was done as a part of Jyväskylä University of Applied Sciences, "software production" and "software production practices", courses. 
                      The client company in our project was Qvantel and our job was to design and implement a project management system. 
                      The project ended up being a Jira dashboard system that contained all the relevant data about the project that the clients teams could need. 
                      The project was done using Atlasboard, so JavaScript, NodeJS, JQL and JQuery made up most of the code.
                      The team comprised of 5 people.
                     </p>
                     </div>
                  </div>
                  </li>
                  <li>
                  <div className="dropdown">
                  <span><a target="_blank" rel="noopener noreferrer" href="https://github.com/FiraxisGlow/Ohjelmistoprojekti-TTMS0900">Thesis evaluation tool - JAMK - Project manager</a></span>
                     <div className="dropdown-content">
                     <p>
                     This project was done as a part of Jyväskylä University of Applied Sciences, "software project", course.
                     The goal of the project was to select a scope from the assignment given by JAMK and produce a software solution within that scope.
                     Our team chose thesis evaluation and produced a digitized tool based on the evaluation model at the time.
                     Project was primarily made with PHP and Javascript.
                     </p>
                     </div> 
                  </div>
                  </li>
                  <li className="last">
                  <div className="dropdown">
                  <span><a target="_blank" rel="noopener noreferrer" href="http://antonkiri.com/Spacelike/index.html">Spacelike Game - Project manager</a></span>
                     <div className="dropdown-content">
                     <p>
                      The project was part of Jyväskylä University of Applied Sciences, "Game development project", course.
                      The project was made using Unity so it's completely based on C#.
                      Aside being the project manager I was responsible for the UI elements and some weapon/powerup design.
                     </p>
                     </div>
                  </div>
                  </li>
              </ul>
          </nav>
        <img className="profileImg" src={profileImage} alt="Me"></img>
     </div>
    );
  }
}

export default Homepage;