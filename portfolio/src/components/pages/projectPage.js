import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import projectImage from '../../Assets/images/dashboard.png';
class Homepage extends Component {
  render() {
    return (
        <div className="container-fluid">
            <img className="projectImg" src={projectImage} alt="Qvantel Dashboard"></img>
            <div className="projectDescription">
                <h2>Dashboard breakdown:</h2>
                <p>
                    This is a screenshot of the final version of the project and it went to beta use as it is here. <br/> 
                    The left column contains widgets for basic project details, project health, and development graphs. <br/> 
                    The basic details widget contains the project name and the next upcoming release date.
                    Project health is calculated from the total amount of issues in the project, closed issues are then subtracted and the remainder is divided by remaining time for the next release. 
                    Development graphs are just burndown charts for each epic for the project. If a project had multiple epics, the widget was animated to display each one. <br/>
                    Center column has widgets for project members and bugs. <br/> 
                    The members widget fetches everyone assigned to the project and sorts them by role. This was done so that everyone in the project knows who's working with who and what role they have. 
                    The bug widget shows all the listed bugs in the project, sorts them by priority, and shows the summary and assignee. It also contains links to the specific issue in Jira. <br/>
                    Right column contains widgets for user stories and testing data.<br/>
                    User stories has the same principle as the bugs widget but it also has the addition of status indication that is concice with the Jira theme.
                    Testing info breaks down testing data by simply showing the amount of passed, failed, and not started tests. 
                    It also has links to each category of tests to Jira so it can be looked up which test has which status.<br/>
                </p>
                <Link to="/">Back</Link>
            </div>
        </div>
    );
  }
}

export default Homepage;