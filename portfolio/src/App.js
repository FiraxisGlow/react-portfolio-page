import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';
// components
import Header from './components/headerComponent/header';
import Footer from './components/footerComponent/footer';
import Homepage from './components/pages/homePage';
import Projectpage from './components/pages/projectPage';
// includes
import './Assets/css/default.min.css';

class App extends Component {
  render() {
    return (
      <Router>
      <div className="App">
      <Header />

        <Route exact path='/' component={Homepage} />
        <Route exact path='/Project' component={Projectpage} />
      
      <Footer />
      </div>
      </Router>
    );
  }
}

export default App;
